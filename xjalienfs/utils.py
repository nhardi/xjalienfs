from xjalienfs import alienAS


async def get_completer_list(websocket):
    await alienAS.websocket.send(alienAS.CreateJsonCommand('ls'))
    ls_result = await alienAS.websocket.recv()
    ls_result = alienAS.json.loads(ls_result)
    ls_list = []
    for element in ls_result['results']:
        list.append(ls_list, element['message'])
    list.append(ls_list, '..')
    return ls_list
